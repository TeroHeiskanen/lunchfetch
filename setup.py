from distutils.core import setup

setup(
    name="lunchfetch",
    version="0.1",
    packages=["lunchfetch"],
    install_requires=[
        "requests",
        "flask",
        "lxml"
    ],
)