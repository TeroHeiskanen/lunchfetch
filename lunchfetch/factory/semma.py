import json
import requests
import logging
from datetime import date
from .abstract import AbstractProvider
from lunchfetch.model import Menu
from lunchfetch.model import Meal
from lunchfetch.model import Restaurant
from lunchfetch.util import TimedCache

_URL = "https://www.semma.fi/api/restaurant/menu/week"
_DATE_GET_FORMAT = "%Y-%m-%d"
_DATE_DATA_FORMAT = "%d.%m.%Y"
_LANG = "fi"


class SemmaProvider(AbstractProvider):

    _RESTAURANTS = {
        "Piato": 207735,
        "Maija": 207659,
    }

    def __init__(self, restaurant: Restaurant, restaurant_page_id: int):
        super().__init__(restaurant)
        self._log = logging.getLogger(__name__)
        self._restaurant_page_id = restaurant_page_id

    @TimedCache(60 * 60)
    def provide(self, date_: date):
        menu = Menu(self.restaurant, date_)
        params = {
            "restaurantPageId": self._restaurant_page_id,
            "language": _LANG,
            "weekDate": date_.strftime(_DATE_GET_FORMAT)
        }
        response = requests.get(_URL, params=params)

        if not response.ok:
            self._log.error("response returned %d" % response.status_code)
            return menu

        try:
            data = json.loads(response.text)

            # find set menus for the day we're asked for
            date_key = date_.strftime(_DATE_DATA_FORMAT)
            s_menus = next((d["SetMenus"] for d in data["LunchMenus"] if d["Date"] == date_key), [])

            # parse Meals from set menus
            for s_menu in s_menus:
                meal = self._parse_s_menu(s_menu)

                if meal is not None:
                    menu.meals.append(meal)
        except (json.JSONDecodeError, KeyError):
            self._log.exception("response parsing failed")

        return menu

    @staticmethod
    def _parse_s_menu(s_menu):
        type_ = s_menu["Name"]
        name = ", ".join(s_meal["Name"] for s_meal in s_menu["Meals"])

        if name == "":
            return None

        return Meal(name, type_)

    @classmethod
    def get_providers(cls):
        return [
            cls(Restaurant("Semma // %s" % name), id_)
            for name, id_ in cls._RESTAURANTS.items()
        ]
