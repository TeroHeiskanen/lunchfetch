import json
import requests
import logging
from datetime import date
from .abstract import AbstractProvider
from lunchfetch.model import Menu
from lunchfetch.model import Meal
from lunchfetch.model import Restaurant
from lunchfetch.util import TimedCache

_URL = "https://www.sodexo.fi/ruokalistat/output/daily_json/{id:d}/{year:d}/{month:d}/{day:d}/{lang:}"
_LANG = "fi"


class SodexoProvider(AbstractProvider):

    _RESTAURANTS = {
        "Mattilanniemi": 66,
    }

    def __init__(self, restaurant: Restaurant, id_: int):
        super().__init__(restaurant)
        self._log = logging.getLogger(__name__)
        self._id = id_

    @TimedCache(60 * 60)
    def provide(self, date_: date):
        menu = Menu(self.restaurant, date_)
        url = _URL.format(
            id=self._id,
            year=date_.year,
            month=date_.month,
            day=date_.day,
            lang=_LANG
        )
        response = requests.get(url)

        if not response.ok:
            self._log.error("Sodexo response returned %d" % response.status_code)
            return menu

        try:
            data = json.loads(response.text)
            for s_course in data["courses"]:
                name = s_course["title_fi"]
                category = s_course["category"]
                meal = Meal(name, category)
                menu.meals.append(meal)
        except (json.JSONDecodeError, KeyError):
            self._log.exception("Sodexo response parsing failed")

        return menu

    @classmethod
    def get_providers(cls):
        return [
            cls(Restaurant("Sodexo // %s" % name), id_)
            for name, id_ in cls._RESTAURANTS.items()
        ]
