from typing import List
from datetime import date
from lunchfetch.model.menu import Menu
from lunchfetch.model.menu import Restaurant


class AbstractProvider:

    def __init__(self, restaurant: Restaurant):
        self.restaurant = restaurant

    def provide(self, date_: date) -> Menu:
        raise NotImplementedError()

    @classmethod
    def get_providers(cls) -> List["AbstractProvider"]:
        raise NotImplementedError()
