import requests
import logging
from datetime import date
from lxml import html
from .abstract import AbstractProvider
from lunchfetch.model import Menu
from lunchfetch.model import Meal
from lunchfetch.model import Restaurant
from lunchfetch.util import TimedCache

_URL = "https://www.raflaamo.fi/fi/{restaurant_page:}/menu"
_DISHES_XPATH = "//ul[contains(@class, 'menu-detail__dish-list')]"
_DISH_XPATH = "li"
_DISH_NAME_XPATH = "div/span[contains(@class, 'menu-detail__dish-name')]"
_DISH_DESCRIPTION_XPATH = "div[contains(@class, 'menu-detail__dish-description')]"


class AbstractRaflaamoProvider(AbstractProvider):

    _RESTAURANTS = {}

    def __init__(self, restaurant: Restaurant, restaurant_page: str, menu_id: int):
        super().__init__(restaurant)
        self._log = logging.getLogger(__name__)
        self._restaurant_page = restaurant_page
        self._menu_id = menu_id

    def _parse_page(self, page: html.HtmlElement, menu: Menu, date_: date) -> None: raise NotImplementedError()
    def _is_valid_date(self, date_: date) -> bool: raise NotImplementedError()

    @TimedCache(60 * 60)
    def provide(self, date_: date):
        menu = Menu(self.restaurant, date_)

        if not self._is_valid_date(date_):
            self._log.debug("no lunch for %r" % date_)
            return menu

        params = {"menuId": self._menu_id}
        url = _URL.format(restaurant_page=self._restaurant_page)
        response = requests.get(url, params)

        if not response.ok:
            self._log.error("response returned %d" % response.status_code)
            return menu

        # doesn't seem to care about the input as long as it is text
        page = html.fromstring(response.text)
        self._parse_page(page, menu, date_)

        return menu

    @classmethod
    def get_providers(cls):
        return [
            cls(Restaurant("Raflaamo // %s" % name), *info)
            for name, info in cls._RESTAURANTS.items()
        ]


class TrattoriaProvider(AbstractRaflaamoProvider):

    _RESTAURANTS = {
        "Trattoria Aukio": ("jyvaskyla/trattoria-aukio-jyvaskyla", 157130),
    }

    def _is_valid_date(self, date_: date) -> bool:
        cur_year, cur_week, cur_weekday = date.today().isocalendar()
        tgt_year, tgt_week, tgt_weekday = date_.isocalendar()

        if (
            (cur_year, cur_week) != (tgt_year, tgt_week) or
            tgt_weekday > 5 or
            cur_weekday > 5
        ):
            self._log.debug("lunch only provided for the current week from monday to friday")
            return False

        return True

    def _parse_page(self, page: html.HtmlElement, menu: Menu, date_: date) -> None:
        try:
            _, _, weekday = date_.isocalendar()
            ul = page.xpath(_DISHES_XPATH)[weekday - 1]
            for div in ul.xpath(_DISH_XPATH):
                name = div.xpath(_DISH_NAME_XPATH)[0].text.strip()
                description = div.xpath(_DISH_DESCRIPTION_XPATH)[0].text.strip()

                meal = Meal("%s (%s)" % (name, description))
                menu.meals.append(meal)
        except IndexError:
            self._log.exception("parsing html failed")

