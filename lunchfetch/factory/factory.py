from concurrent.futures import ThreadPoolExecutor
from typing import List
from datetime import date
from lunchfetch.model import Restaurant
from lunchfetch.model import Menu
from .semma import SemmaProvider
from .sodexo import SodexoProvider
from .raflaamo import TrattoriaProvider

_PROVIDER_CLASSES = [
    SemmaProvider,
    SodexoProvider,
    TrattoriaProvider,
]


class LunchFactory:
    _PROVIDERS = None

    @classmethod
    def _get_providers(cls):
        if cls._PROVIDERS is None:
            cls._PROVIDERS = [p for p_cls in _PROVIDER_CLASSES for p in p_cls.get_providers()]
        return cls._PROVIDERS

    @classmethod
    def get_restaurants(cls) -> List[Restaurant]:
        return [p.restaurant for p in cls._get_providers()]

    @classmethod
    def get_menus(cls, date_: date) -> List[Menu]:
        executor = ThreadPoolExecutor()
        futures = [executor.submit(p.provide, date_) for p in cls._get_providers()]
        return [f.result() for f in futures]
