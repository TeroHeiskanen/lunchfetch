import json
import logging
from datetime import date
from flask import Blueprint
from flask import abort
from lunchfetch.factory import LunchFactory
from lunchfetch.model import JSONEncoder

bp = Blueprint("api", __name__, url_prefix="/api")
log = logging.getLogger(__name__)


@bp.route("/restaurants")
def get_restaurants():
    return json.dumps(LunchFactory.get_restaurants(), cls=JSONEncoder)


@bp.route("/menus")
def get_menus_for_today():
    return json.dumps(LunchFactory.get_menus(date.today()), cls=JSONEncoder)


@bp.route("/menus/<date_string>")
def get_menus_for_date(date_string):
    try:
        date_ = date.fromisoformat(date_string)
        return json.dumps(LunchFactory.get_menus(date_), cls=JSONEncoder)
    except ValueError:
        log.exception("parsing datestring failed")
        abort(400)
