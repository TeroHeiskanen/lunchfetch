from flask import Flask


def create_app():
    app = Flask(__name__, instance_relative_config=True)

    from . import api
    from . import index
    app.register_blueprint(api.bp)
    app.register_blueprint(index.bp)

    return app
