import logging
from flask import Blueprint
from flask import render_template

bp = Blueprint("", __name__)


@bp.route("/")
def index():
    return render_template("index.html")


log = logging.getLogger(__name__)