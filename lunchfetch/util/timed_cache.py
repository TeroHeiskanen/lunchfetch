import time
from functools import partial


class TimedCache:

    def __init__(self, timeout: int):
        self._cache = {}
        self._cached_at = None
        self._timeout = timeout

    def __call__(self, func):
        def wrap(*args, **kwargs):
            key = (args, *kwargs.keys())
            cache = self._get_cache(key)

            if cache is None:
                self._cache[key] = cache = func(*args, **kwargs)
                self._cached_at = time.time()

            return cache
        return wrap

    def _get_cache(self, key):
        if key not in self._cache:
            return None

        if time.time() - self._cached_at >= self._timeout:
            self._cache[key] = None

        return self._cache[key]

    def __get__(self, instance, owner):
        return partial(self.__call__, instance)