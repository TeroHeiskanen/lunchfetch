import json
import datetime
from .restaurant import Restaurant
from .meal import Meal
from .menu import Menu


class JSONEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, (Restaurant, Meal, Menu)):
            return obj.__dict__

        if isinstance(obj, (datetime.datetime, datetime.date)):
            return obj.isoformat()

        return super().default(obj)
