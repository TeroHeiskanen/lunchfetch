from datetime import date
from typing import List

from .meal import Meal
from .restaurant import Restaurant


class Menu:
    def __init__(self,  restaurant: Restaurant, date_: date, meals: List[Meal] = None):
        self.restaurant = restaurant
        self.meals = meals or []
        self.date = date_

    def __str__(self):
        return "%s (%s)" % (self.restaurant, self.date)
