

class Meal:
    def __init__(self, name: str, category: str = None):
        self.name = name.capitalize()
        self.category = None

        if category is not None:
            self.category = category.capitalize()

    def __str__(self):
        return "%s (%s)" % (self.name, self.category)
